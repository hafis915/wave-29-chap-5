const http = require('http')
const port = 4000
const fs = require('fs')
const url = require('url')

const server = http.createServer((req, res) => {
        try {
            const { pathname } = url.parse(req.url)
            const method = req.method
             
            switch (method) {
                case 'GET':
                    switch (pathname) {
                        case '/':
                            res.setHeader('Content-Type', 'text/html')
                            const data = fs.readFileSync('./index.html')
                            if(data) {
                                res.writeHead(200)
                                res.end(data)
                            }
                            break;
                        case '/user':
                            res.setHeader('Content-Type', 'text/html')
                            const user = fs.readFileSync('./user.html', null)
                            if(user) {
                                res.writeHead(200)
                                res.end(user)
                            }
                            break
                        case '/objek':
                            res.setHeader('Content-Type', 'application/json')
                            res.writeHead(200)
                            const payload = {
                                name : 'binar'
                            }
                            res.end(JSON.stringify(payload))

                            break
                        default:
                            res.writeHead(404)
                            res.end('Page Not Found')
                            break;
                    }
    
                    break;
                case 'POST':
                    res.setHeader('Content-Type', 'application/json')

                    let body = ''
                    req
                    .on('data', (chunk) => {
                        console.log(chunk)
                        body += chunk
                        console.log(body, "data")
                    })
                    .on('end',() => {

                        console.log(body)
                        res.end('end')
                    })
                    break
                case 'PUT' :
                    break;
                case 'DELETE':
                    break
                default:
                    break;
            }
        } catch (error) {
            console.log(error)
            res.end('file not found')
        }

      
})


server.listen(port, () => {
    console.log('server start')
})


