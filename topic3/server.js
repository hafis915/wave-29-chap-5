const express = require('express')
const app = express()
const port = 3000
const router = require('./routes')
const { logger, errorHandler }  = require('./middleware')

app.use('/static', express.static(__dirname + '/public'))
app.set('view engine', 'ejs')
app.use(express.json())
app.use(express.urlencoded({extended:true}))
// application level middleware


app.use(logger)

app.use(router)
app.use(errorHandler)

app.listen(port, () => {
    console.log('success run on port ' + port)
})