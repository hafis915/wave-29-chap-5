function logger (req,res,next) {
    console.log(req.method, req.url)
    next()
}

function checkAdmin(req,res,next) {
    try {
        // cek isi headers
        const headers = req.headers
        const role = headers.role

        if(role.toLowerCase() === 'admin') {
            next()
        }else {
            console.log('==== kondisi 401 ====')
            res.status(401).json({
                message : 'Unauthorize Access'
            })
        }

    } catch (error) {
        
    }
}

function errorHandler(err,req, res, next) {
    console.log(err, '<< ini err')
    if(err.status) {
        res.status(err.status).json({
            message : err.message
        })
    }else {
        res.status(500).json({
            message : 'SERVER ERROR'
        })
    }

}

module.exports = {
    logger,
    checkAdmin,
    errorHandler
}