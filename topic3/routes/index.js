const express = require('express')
const router = express.Router()
const HomeController = require('../controller')
const { checkAdmin } = require('../middleware')


// middleware
// router.use(checkAdmin) // semua router dibawa akan masuk ke middleware terlebih dahulu

router.get('/home' ,HomeController.getHome )
router.post('/test', HomeController.postTest)
router.get('/contact' ,HomeController.renderContact )
router.post('/',checkAdmin,  HomeController.postHome ) // cuman 1 route yang pakai middleware
module.exports = router 