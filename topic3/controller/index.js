const user = require('../Model/user.json')
class HomeController {

    static async postTest(req,res,next) {
        try {
            console.log('=== ini post ===')
            console.log(req.body)
            res.redirect('/contact')
        } catch (error) {
            
        }
    }
    static async getHome(req,res, next)  {
        try {       
            const data = user['users'][0]
            res.render('home', {data : data, isHide : true , arr : user['users'] } )
        } catch (error) {
            next(error)
        }
    }

    static async renderContact(req,res,next) {
        try {
            res.render('contact')
        } catch (error) {
            next(error)
        }
    }
    static async postHome(req,res , next) { 
        try {
            const body = req.body
            if(!body.name)  {
                throw {
                    status : 400,
                    message : 'Name is required'
                }
            }else{
                res.status(200).json({name : 'success'})
            }
        } catch (error) {
            console.log('=== masuk ke catch ===')
            next(error)
        }
    }
}

module.exports =  HomeController